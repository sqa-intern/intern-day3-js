console.log('Hello World');
console.error('this is an eror');
console.warn('this is a warning');

//let, const

const score = 30;

console.log('30')

// String, Numbers, Boolean, null, undefined

// const name = 'key';
// const age = 30;
// const rating = 4.5;
// const isCool = true;
// const x = null;
// const y = undefined;
// let z;

console.log(typeof rating);

// String, Numbers, Boolean, null, undefined

const name = 'hasyim,';
const age = 30;

// concatenation
console.log('my name is ' + name + ' I am ' + age)
// Template String
const hello = 'my name is ${name} and I am ${age}';

const s = 'Technologi, computers, it, code';
console.log(s.split( ));

// Arrays - variables that hold multiple values
const fruits = ['apples', 'oranges', 'pears'];

fruits[3] = 'grapes';
fruits.push ('mangos');
fruits.unshift ('strawberries');

console.log(fruits);

// OBJECT LITERALS
const person = {
    firstName: 'Yustika',
    age: 24,
    hobbies: ['music', 'movies', 'sports'],
    address: {
      street: '01 Lukas st',
      city: 'Cilacap',
      state: 'Ina'

    }
  }
  console.log(person);

  // Array of objects
const todos = [
    {
      id: 1,
      text: 'gymnastic',
      isComplete: false
    },
    {
      id: 2,
      text: 'Dinner with boyfriend',
      isComplete: false
    },
    {
      id: 3,
      text: 'Meeting with boss',
      isComplete: true
    }
  ];
  console.log(todos);

  
  const todoJSON = JSON.stringify(todos);

  console.log(todoJSON);

  // For
for(let i = 0; i <= 10; i++){
    console.log(`For Loop Number: ${i}`);
  }
  
  // While
  let i = 0
  while(i <= 10) {
    console.log(`While Loop Number: ${i}`);
    i++;
  }

  // forEach() - Loops through array
todos.forEach(function(todo, i, myTodos) {
    console.log(`${i + 1}: ${todo.text}`);
    console.log(myTodos);
  });
  
  // map() - Loop through and create new array
  const todoTextArray = todos.map(function(todo) {
    return todo.text;
  });
  
  console.log(todoTextArray);


  // forEach map, filter
const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === true;
}).map(function(todo) {
   return todo.text;
  })

  console.log(todoCompleted);

const x = 6;
const y = 11;

if(x > 5 && y > 11) {
    console.log('x more than 5 or y more than 10');
}

// constructor Function
function Person(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
    this.getBirthDay = function() {
        return this.dob.getFullYear();
    }
    this.getFullName = function() {
        return `${this.firstName} ${this.lastName}`;
    }
}

// Instantiate an object from the class
const person1 = new Person('John', 'Doe', '7-8-80');
const person2 = new Person('Steve', 'Smith', '8-2-90');

console.log(person1);



